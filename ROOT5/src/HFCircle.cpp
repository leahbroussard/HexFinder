// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFCircle.hh
// Purpose: Implements circle geometry

#ifndef HFCIRCLE_CPP__
#define HFCIRCLE_CPP__

#include "HFCircle.hh"

/*************************************************************************/
//                                Draw
/*************************************************************************/
void HFCircle::Draw() {
	if (radius == 0) return;
	ell = new TEllipse(center.x, center.y, radius, radius);
	ell->SetFillStyle(0);
	ell->Draw("same");
}

/*************************************************************************/
//                              IsInside
/*************************************************************************/
bool HFCircle::IsInside(HF2DPoint pnt) {
	if (radius == 0) return false;
	if ((pnt.x-center.x)*(pnt.x-center.x)+(pnt.y-center.y)*(pnt.y-center.y) < radius*radius)
		return true;
	return false;
}



#endif // HFCIRCLE_CPP__