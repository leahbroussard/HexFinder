// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFHexArray.hh
// Purpose: Handles the hexagon "math"

#ifndef HFHEXARRAY_CPP__
#define HFHEXARRAY_CPP__

#include "HFHexArray.hh"
#if defined (__CINT__)
#include "TMath.h"
#endif

/*************************************************************************/
//                             Constructor
/*************************************************************************/
HFHexArray::HFHexArray() {
	init();
}

HFHexArray::HFHexArray(int nrings, bool flattop, double space, HF2DPoint neworig) {
	init();
	CreateHexagon(nrings, flattop, space, neworig);
}


HFHexArray::init() {
	spacing = 0;
	origin.x = 0;
	origin.y = 0;
}

/*************************************************************************/
//                              Destructor
/*************************************************************************/
HFHexArray::~HFHexArray() {
}


/*************************************************************************/
//                                Reset
/*************************************************************************/
void HFHexArray::Reset() {
	origin.x = 0;
	origin.y = 0;
}


/*************************************************************************/
//                            CreateHexagon
/*************************************************************************/
void HFHexArray::CreateHexagon(int NN, bool flattop, double space, HF2DPoint neworig) {
	Reset();
	origin = neworig;
	flattoporientation = flattop;
	spacing = space;
	int N = 6; //quick hack so I can use fixed array sizes
	int dim = 2*N + 1;
	idxQ = N;
	idxR = N;
	int idx = 0;
	for (int x=0;x<dim;x++) {
		for (int y=dim-1;y>=0;y--) {
			HFHexPoint pt; pt.q = (x - idxQ); pt.r = (y - idxR);
			myHashTable[x][y] = -1;
			if (pt.q + pt.r <= N && pt.q + pt.r >= -1.*N) {
				myHexPtArray[idx] = pt;
				myHashTable[x][y] = idx++;
			}
		}
	}
	CalcCoords();
	CalcNeighbors();
}

/*************************************************************************/
//                             CalcCoords
/*************************************************************************/
void HFHexArray::CalcCoords() {
	for (int i=0;i<127;i++) {
		coords[i] = GetXY(i);
	}
}

/*************************************************************************/
//                            CalcNeighbors
/*************************************************************************/
void HFHexArray::CalcNeighbors() {
	for (int i=0;i<127;i++) {
		for (int j=0;j<127;j++) {
			neighbors[i][j] = false;
		}
		for (int dq=-1;dq<=1;dq++) {
			for (int dr=-1;dr<=1;dr++) {
				if (dr != dq) {
					HFHexPoint pt; pt.q = (myHexPtArray[i].q + dq); pt.r = (myHexPtArray[i].r + dr);
					int idx = GetIndex(pt);
					if (idx != -1) {
						neighbors[i][idx] = true;
					}
				}
			}
		}
	}
}

/*************************************************************************/
//                              GetXY
/*************************************************************************/
HF2DPoint HFHexArray::GetXY(int idx) {
	HF2DPoint x;
	if (idx < 0 || idx > 126) {cout << "BAD" << endl; return x;}
	return GetXY(myHexPtArray[idx]);
}

HF2DPoint HFHexArray::GetXY(HFHexPoint hxpt) {
	HF2DPoint pnt;
	if (flattoporientation) {
		pnt.x = (spacing/2.) * sqrt(3.) * (hxpt.q + hxpt.r/2.) + origin.x;
		pnt.y = (spacing/2.) * (3./2.) * hxpt.r + origin.y;
	}
	else {
		pnt.x = (spacing/2.) * (3./2.) * hxpt.q + origin.x;
		pnt.y = (spacing/2.) * sqrt(3.) * (hxpt.r + hxpt.q/2.) + origin.y;
	}
	return pnt;
}

/*************************************************************************/
//                              GetIndex
/*************************************************************************/
int HFHexArray::GetIndex(HFHexPoint pt) {
	if (pt.q+idxQ < 0 || pt.q+idxQ >= 13) return -1;
	if (pt.r+idxR < 0 || pt.r+idxR >= 13) return -1;
	return myHashTable[pt.q+idxQ][pt.r+idxR];
}

int HFHexArray::GetIndex(HF2DPoint pnt) {
	double q,r,s;
	double side = spacing/2.;
	if (!flattoporientation) {
		q = (pnt.x-origin.x) * 2./3./side;
		r = (-1.*(pnt.x-origin.x)/3. + (pnt.y-origin.y)*sqrt(3.)/3.)/side;
	}
	else {
		q = (-1.*(pnt.y-origin.y)/3. + (pnt.x-origin.x)*sqrt(3.)/3.)/side;
		r = (pnt.y-origin.y) * 2./3./side;
	}
	s = -1.*q - 1.*r;
#if !defined (__CINT__)
	double rq = round(q); double dq = fabs(rq - q);
	double rr = round(r); double dr = fabs(rr - r);
	double rs = round(s); double ds = fabs(rs - s);
#else
	double rq = TMath::Nint(q); double dq = TMath::Abs(rq - q);
	double rr = TMath::Nint(r); double dr = TMath::Abs(rr - r);
	double rs = TMath::Nint(s); double ds = TMath::Abs(rs - s);
#endif
	if (dq > dr && dq > ds)
		rq = -1.*rr - 1.*rs;
	else if (dr > ds)
		rr = -1.*rq - 1.*rs;
	else
		rs = -1.*rr - 1.*rq;
	HFHexPoint hpt; hpt.q = rq; hpt.r = rr;
	return GetIndex(hpt);
}

#endif // HFHEXARRAY_CPP__