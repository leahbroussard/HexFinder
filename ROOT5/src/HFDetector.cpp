// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFDetector.cpp
// Purpose: Example of detector class which owns array of pixels, used here just for drawing

#ifndef HFDETECTOR_CPP__
#define HFDETECTOR_CPP__

#include <string>
#include "HFDetector.hh"

/*************************************************************************/
//                            Constructor
/*************************************************************************/
HFDetector::HFDetector() {
	Reset();
}

/*************************************************************************/
//                             Destructor
/*************************************************************************/
HFDetector::~HFDetector() {
}

/*************************************************************************/
//                                Reset
/*************************************************************************/
void HFDetector::Reset() {
}

/*************************************************************************/
//                             BuildPixels
/*************************************************************************/
void HFDetector::BuildPixels(HF2DPoint* coords, double side, bool flattop) {
	if (coords == 0) return;  
	//Note:  code will crash if coords is not an array of 127
	for (int i=0;i<127;i++) {
		myPixArray[i].Set(side,flattop);
		myPixArray[i].SetCenter(coords[i]);
		char label[255];
		sprintf(label,"%d",i+1);
		myPixArray[i].SetLabel(label);
	}
}

/*************************************************************************/
//                                 Draw
/*************************************************************************/
void HFDetector::Draw() {
	myShape.Draw();
	for (int i=0;i<127;i++) {
		myPixArray[i].Draw();
	}
}

#endif // HFDETECTOR_CPP__
