// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFShape.hh
// Purpose: Parent class for shapes

#ifndef HFSHAPE_HH__
#define HFSHAPE_HH__

#include <stdlib.h>
#include <iostream>
#include <cmath>

struct HF2DPoint {
	double x; 
	double y;
	//HF2DPoint& operator=(HF2DPoint pt) {this->swap(pt);return *this;}
	//void swap(HF2DPoint &pt) {std::swap(x,pt.x); std::swap(y,pt.y);}
	//HF2DPoint(double xx=0, double yy=0):x(xx),y(yy){}
};

enum HFShapeType {HFCircleType, HFHexFlatTopType, HFHexPointyTopType};

class HFShape{
	protected:
		HF2DPoint center;
	public:
		HFShape() {};
		~HFShape() {};
		virtual void SetCenter(HF2DPoint ctr) {center = ctr;};
		HF2DPoint GetCenter() {return center;};
		virtual double GetDim() {return -1;};
		virtual void SetDim(double dim) {};
		virtual void Draw() {};
		virtual bool IsInside(HF2DPoint pnt) {return false;};
};



#endif // HFSHAPE_HH__