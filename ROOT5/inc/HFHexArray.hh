// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFHexArray.hh
// Purpose: Handles the hexagon "math"

#ifndef HFHEXARRAY_HH__
#define HFHEXARRAY_HH__

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <cmath>

#include "HFShape.hh"

using std::vector;


class HFHexArray {
	private:
		HF2DPoint origin;					//(X,Y) of center of detector
		HF2DPoint coords[127];				//(X,Y) of center of each pixel
		bool neighbors[127][127];			//are pixels i and j neighbors?
		struct HFHexPoint {					//Hex "axial" coordinates
			int q; 
			int r;
			//HFHexPoint(int qq=0, int rr=0):q(qq),r(rr){}
		};
		HFHexPoint myHexPtArray[127];		//axial coordinates of each pixel
		int myHashTable[13][13];			//convert axial coord. to pixel index
		int idxQ, idxR;						//used for axial coord. conversion
		double spacing;						//distance between center of pixels
		bool flattoporientation;			//hexagon flattop looks like <=>
	public:
		HFHexArray();
		HFHexArray(int NPixelRings, bool flattop, double space, HF2DPoint neworig);
		~HFHexArray();
		void Reset();
		void CreateHexagon(int NPixelRings, bool flattop, double space, HF2DPoint neworig);
		HF2DPoint* GetCoords() {return &coords[0];};
		bool* GetNeighbors() {return &neighbors[0][0];};	//not ideal...
		HF2DPoint GetXY(int idx);			//(X,Y) of given pixel center
		int GetPixel(HF2DPoint pt) {return GetIndex(pt) + 1;};
	private:
		void init();
		void CalcCoords();
		void CalcNeighbors();
		HF2DPoint GetXY(HFHexPoint hxpt);
		int GetIndex(HFHexPoint pt);
		int GetIndex(HF2DPoint pt);
};



#endif // HFHEXARRAY_HH__