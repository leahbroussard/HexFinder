// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFCircle.hh
// Purpose: Implements circle geometry

#ifndef HFCIRCLE_HH__
#define HFCIRCLE_HH__

#include <stdlib.h>
#include <iostream>
#include <cmath>
 
#include "HFShape.hh"
#include "TEllipse.h"	//from ROOT

class HFCircle : public HFShape{
	private:
		double radius;
		TEllipse* ell;
	public:
		HFCircle() {radius = 0; ell = 0;};
		HFCircle(double dim) {radius = dim; ell = 0;};
		~HFCircle() {if (ell != 0) delete ell;};
		double GetDim() {return radius;};
		void SetDim(double dim) {radius = dim;};
		void Draw();
		bool IsInside(HF2DPoint pnt);
};



#endif // HFCIRCLE_HH__