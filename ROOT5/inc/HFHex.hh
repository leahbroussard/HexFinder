// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFHex.hh
// Purpose: Implements hexagon geometry

#ifndef HFHEX_HH__
#define HFHEX_HH__

#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <string>
 
#include "HFShape.hh"
#include "TPolyLine.h"	//from ROOT
#include "TText.h"		//from ROOT

using std::vector;

class HFHex : public HFShape{
	private:
		bool FlatTopOrientation;
		double side;
		HF2DPoint pts[6];
		TPolyLine* pline;
		TText* label;
	public:
		HFHex();
		HFHex(double dim, bool flattop);
		~HFHex();
		void Get(double& sd, bool& flattop);
		void Set(double sd, bool flattop);
		double GetDim() {return side;};
		void SetLabel(std::string lb);
		void Draw();
		bool IsInside(HF2DPoint pnt);
	private:
		void CalcCorners();
};



#endif // HFHEX_HH__