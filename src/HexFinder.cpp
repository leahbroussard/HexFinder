// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HexFinder.cpp
// Purpose: Simple code to demonstrate using HFHexArray to quickly calculate which pixel is at some (X,Y)

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <cstring>

#include "HFHexArray.hh"
#include "HFDetector.hh"

#include "TApplication.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TMarker.h"
#include "TRandom3.h"

using std::cout;
using std::cin;
using std::endl;

const double mm = 1.;

/*************************************************************************/
//                            Main Function
/*************************************************************************/
int main (int argc, char *argv[]) {
	cout << endl << "Welcome to HexFinder v1.0" << endl << endl;
	
	//interface to ROOT
	TApplication* myapp = new TApplication("myapp",0,0);
	gStyle->SetOptStat(0);
	
	//Initialize detector specs
	bool FlatTop = false;				//orientation of hexagon array
	int NPixelRings = 6;				//rings surrounding central pixel
	double ContactSpacing = 10.4*mm;		//"outer" diamter of hex pixel
	HF2DPoint origin(0,0);				//location of detector center
	double DetRadius = ContactSpacing*(2.*NPixelRings+1)/2.;  //radius of detector

	//HFHexArray handles "hexagon math"
	HFHexArray myHexArray(NPixelRings,FlatTop,ContactSpacing,origin);

	//Example application: draw detector and pixels
	HFDetector myDetector;
	myDetector.SetCoord(origin);
	myDetector.SetRadius(DetRadius);
	myDetector.BuildPixels(myHexArray.GetCoords(),ContactSpacing/2.,!FlatTop);
	//Draw detector
	TCanvas c1("c","c",800,800);
	c1.SetGridx();
	c1.SetGridy();
	TH2D dummy("dummy","Detector Geometry",DetRadius*2,-1.*DetRadius,DetRadius,DetRadius*2,-1.*DetRadius,DetRadius);
	dummy.Draw();
	myDetector.Draw();
	
	TRandom3 dice(0);
	
	//Case 1: identify which pixel each point resides in using HFHexArray::GetPixel
	vector<HF2DPoint> pnt(30);
	for (int i=0;i<pnt.size();i++) {
		pnt[i].x = -1.*DetRadius + dice.Rndm()*DetRadius*2.;
		pnt[i].y = -1.*DetRadius + dice.Rndm()*DetRadius*2.;
	}
	
	for (int i=0;i<pnt.size();i++) {
		TMarker* mm = new TMarker(pnt[i].x,pnt[i].y,24);
		mm->SetMarkerColor(kBlue);
		mm->SetMarkerSize(4);
		mm->Draw("same");
		std::string label = std::to_string(myHexArray.GetPixel(pnt[i]));
		TText* tt = new TText(pnt[i].x,pnt[i].y,label.c_str());
		tt->SetTextAlign(22);
		tt->SetTextColor(kBlue);
		tt->SetTextSize(0.04);
		tt->Draw("same");
	}
	
	//Case 2: check if a point is within specific pixel "64" using "HFHex::IsInside"
	int chosenpixel = 64;
	pnt.resize(100);
	for (int i=0;i<pnt.size();i++) {
		pnt[i].x = -1.*DetRadius + dice.Rndm()*DetRadius*2.;
		pnt[i].y = -1.*DetRadius + dice.Rndm()*DetRadius*2.;
	}
	for (int i=0;i<pnt.size();i++) {
		TMarker* mm = new TMarker(pnt[i].x,pnt[i].y,20);
		if (myDetector.InsidePixel(chosenpixel,pnt[i]))
			mm->SetMarkerColor(kGreen);
		else
			mm->SetMarkerColor(kRed);
		mm->Draw("same");
	}

	cout << "Hold ctrl+C to quit" << endl;
	myapp->Run();
}
