// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFDetector.cpp
// Purpose: Example of detector class which owns array of pixels, used here just for drawing

#ifndef HFDETECTOR_CPP__
#define HFDETECTOR_CPP__

#include <string>
#include "HFDetector.hh"

/*************************************************************************/
//                            Constructor
/*************************************************************************/
HFDetector::HFDetector() {
	Reset();
}

/*************************************************************************/
//                             Destructor
/*************************************************************************/
HFDetector::~HFDetector() {
}

/*************************************************************************/
//                                Reset
/*************************************************************************/
void HFDetector::Reset() {
	myPixArray.clear();
}

/*************************************************************************/
//                             BuildPixels
/*************************************************************************/
void HFDetector::BuildPixels(vector<HF2DPoint> coords, double side, bool flattop) {
	myPixArray.resize(coords.size());
	for (int i=0;i<myPixArray.size();i++) {
		myPixArray[i].Set(side,flattop);
		myPixArray[i].SetCenter(coords[i]);
		myPixArray[i].SetLabel(std::to_string(i+1));
	}
}

/*************************************************************************/
//                                 Draw
/*************************************************************************/
void HFDetector::Draw() {
	myShape.Draw();
	for (int i=0;i<myPixArray.size();i++) {
		myPixArray[i].Draw();
	}
}

#endif // HFDETECTOR_CPP__
