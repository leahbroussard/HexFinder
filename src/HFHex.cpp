// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFHex.hh
// Purpose: Implements hexagon geometry

#ifndef HFHEX_CPP__
#define HFHEX_CPP__

#include "HFHex.hh"

/*************************************************************************/
//                             Constructor
/*************************************************************************/
HFHex::HFHex() {
}

HFHex::HFHex(double sd, bool flattop) {
	Set(sd, flattop);
};


/*************************************************************************/
//                            Getter/Setter
/*************************************************************************/
void HFHex::Get(double& sd, bool& flattop) {
	sd = side;
	flattop = FlatTopOrientation;
};

void HFHex::Set(double sd, bool flattop) {
	side = sd;
	FlatTopOrientation = flattop;
	CalcCorners();
};

void HFHex::SetLabel(std::string lb) {
	//minor bug, must come after center is set
	label.reset(std::move(new TText(center.x,center.y,lb.c_str())));
	label->SetTextAlign(22);
	label->SetTextSize(0.03);
}

/*************************************************************************/
//                             CalcCorners
/*************************************************************************/
void HFHex::CalcCorners() {
	pts.resize(6);
	pts[0].x = -1.*side/2.;		pts[0].y = side*sqrt(3.)/2.;
	pts[1].x = side/2.;			pts[1].y = side*sqrt(3.)/2.;
	pts[2].x = side; 			pts[2].y = 0;
	pts[3].x = side/2.; 		pts[3].y = -1.*side*sqrt(3.)/2.;
	pts[4].x = -1.*side/2.; 	pts[4].y = -1.*side*sqrt(3.)/2.;
	pts[5].x = -1.*side;		pts[5].y = 0;
	if (!FlatTopOrientation) {
		for (int i=0;i<pts.size();i++) {
			double tmp = pts[i].x;
			pts[i].x = pts[i].y;
			pts[i].y = tmp;
		}	
	}
}

/*************************************************************************/
//                                Draw
/*************************************************************************/
void HFHex::Draw() {
	if (side == 0) return;
	double x[7];
	double y[7];
	for (int i=0;i<7;i++) {
		x[i] = pts[i%6].x + center.x;
		y[i] = pts[i%6].y + center.y;
	}
	pline.reset(std::move(new TPolyLine(7,x,y)));
	pline->SetFillStyle(0);
	pline->SetLineWidth(2);
	pline->Draw("same");
	label->Draw("same");
}

/*************************************************************************/
//                              IsInside
/*************************************************************************/
bool HFHex::IsInside(HF2DPoint pnt) {
	if (side == 0) return false;
	double q;
	double r;
	if (FlatTopOrientation) {
		q = (pnt.x-center.x) * 2./3./side;
		r = (-1.*(pnt.x-center.x)/3. + (pnt.y-center.y)*sqrt(3.)/3.)/side;
	}
	else {
		q = (-1.*(pnt.y-center.y)/3. + (pnt.x-center.x)*sqrt(3.)/3.)/side;
		r = (pnt.y-center.y) * 2./3./side;
	}
	double s = -1.*q - 1.*r;
	double rq = round(q); double dq = fabs(rq - q);
	double rr = round(r); double dr = fabs(rr - r);
	double rs = round(s); double ds = fabs(rs - s);
	if (dq > dr && dq > ds)
		rq = -1.*rr - 1.*rs;
	else if (dr > ds)
		rr = -1.*rq - 1.*rs;
	else
		rs = -1.*rr - 1.*rq;
	
	if (abs(rq) < 1 && abs(rr) < 1 && abs(rs) < 1) {
		return true;
	}
	return false;
}

#endif // HFHEX_CPP__