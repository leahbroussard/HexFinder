// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFHexArray.hh
// Purpose: Handles the hexagon "math"

#ifndef HFHEXARRAY_HH__
#define HFHEXARRAY_HH__

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <cmath>

#include "HFShape.hh"

using std::vector;


class HFHexArray {
	private:
		HF2DPoint origin;					//(X,Y) of center of detector
		vector<HF2DPoint> coords;			//(X,Y) of center of each pixel
		vector<vector<bool>> neighbors;		//are pixels i and j neighbors?
		struct HFHexPoint {					//Hex "axial" coordinates
			int q; 
			int r;
			HFHexPoint(int qq=0, int rr=0):q(qq),r(rr){}
		};
		vector<HFHexPoint> myHexPtArray;	//axial coordinates of each pixel
		vector<vector<int> > myHashTable;	//convert axial coord. to pixel index
		int idxQ, idxR;						//used for axial coord. conversion
		double spacing;						//distance between center of pixels
		bool flattoporientation;			//hexagon flattop looks like <=>
	public:
		HFHexArray();
		HFHexArray(int nrings, bool flattop, double space, HF2DPoint neworig);
		~HFHexArray();
		void Reset();
		void CreateHexagon(int nrings, bool flattop, double space, HF2DPoint neworig);
		vector<HF2DPoint> GetCoords() {return coords;};
		vector<vector<bool>> GetNeighbors() {return neighbors;};
		HF2DPoint GetXY(int idx);			//(X,Y) of given pixel center
		int GetPixel(HF2DPoint pt) {return GetIndex(pt) + 1;};
	private:
		void CalcCoords();
		void CalcNeighbors();
		HF2DPoint GetXY(HFHexPoint hxpt);
		int GetIndex(HFHexPoint pt);
		int GetIndex(HF2DPoint pt);
};



#endif // HFHEXARRAY_HH__