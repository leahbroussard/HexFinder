// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFCircle.hh
// Purpose: Implements circle geometry

#ifndef HFCIRCLE_HH__
#define HFCIRCLE_HH__

#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <memory>
 
#include "HFShape.hh"
#include "TEllipse.h"	//from ROOT

class HFCircle : public HFShape{
	private:
		double radius;
		std::unique_ptr<TEllipse> ell;
	public:
		HFCircle() {};
		HFCircle(double dim) {radius = dim;};
		~HFCircle() {};
		HFCircle(const HFCircle& hex) = delete;
		HFCircle& operator=(const HFCircle& hex) = delete;
		HFCircle(HFCircle &&) = default;
		HFCircle& operator=(HFCircle&& hex) = default;
		double GetDim() {return radius;};
		void SetDim(double dim) {radius = dim;};
		void Draw();
		bool IsInside(HF2DPoint pnt);
};



#endif // HFCIRCLE_HH__