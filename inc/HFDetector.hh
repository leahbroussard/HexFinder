// Copyright 2017.  UT-Battelle, LLC.  All rights reserved.
// This file is part of HexFinder
// See LICENSE.md included in top directory of this distribution.

// File: HFDetector.hh
// Purpose: Example of detector class which owns array of pixels, used here just for drawing

#ifndef HFDETECTOR_HH__
#define HFDETECTOR_HH__

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <cmath>

using std::vector;
using std::cout;
using std::endl;

#include "HFCircle.hh"
#include "HFHex.hh"

class HFDetector{
	private:
		HF2DPoint coord;					//Center of detector to draw
		HFCircle myShape;					//Detector is a circle
		vector<HFHex> myPixArray;			//Pixels are hexagons
	public:
		HFDetector();
		~HFDetector();
		void Reset();
		HF2DPoint GetCenter() {return myShape.GetCenter();};
		void SetCoord(HF2DPoint c) {coord = c;};
		void SetRadius(double rad) {myShape.SetDim(rad);};
		void BuildPixels(vector<HF2DPoint> coords, double side, bool flattop);
		bool InsidePixel(int idx, HF2DPoint pnt) {return myPixArray[idx-1].IsInside(pnt);};
		void Draw();
	private:
		vector<int> GetNearestPixels(HF2DPoint pnt);
};



#endif // HFDETECTOR_HH__