HexFinder
=============

This code demonstrates using HFHexArray class to build a hexagonal array of pixels, determine which pixels are neighbors, and convert between pixel number and (X,Y) coordinate position (below: blue circled numbers).

Alternatively, the HFHex class demonstrates deciding if it contains an (X,Y) coordinate independent of any indexing of the full array (below: green and red dots).

Example output:

![](HexFinder.png "Example output")

Please see the excellent guide at http://www.redblobgames.com/grids/hexagons/.

Requirements
------------

This code was written in C++ and tested using GNU make version 3.81, gcc version 4.8.4, and ROOT v5 and v6.  CINT (interactive ROOT 5) does not support some of the modern C++ features used in this code so an alternative version is provided in the ROOT5 folder (loaded automatically by .rootlogon.C).

Directions
----------

To build the code, simply run

```
$ make clean
$ make
$ ./HexFinder
```

from the top directory of the distribution.  Header files are in the inc/ directory and source code is in the src/ directory.

To run the ROOT script, open the ROOT environment and run

```
$ .L HexFinder.C
$ HexFinder()
```

from the top directory of the distribution.  A .rootlogon.C script is provided.


Acknowledgements
----------------

HexFinder is based on work supported by the U.S. Department of Energy, Office of Science, Office of Nuclear Physics.

Copyright and license
---------------------
Copyright 2017 UT-Battelle, LLC. All rights reserved.